#!/bin/sh

groupadd -g $DOCKER_GROUP_ID docker
usermod -a -G docker ouroboros

exec runuser -u ouroboros -- ouroboros $@
