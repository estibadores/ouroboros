FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            python3 python3-pip python3-docker python3-prometheus-client \
            python3-requests python3-influxdb python3-apscheduler \
            python3-setuptools && \
    apt-get clean

RUN pip3 install --no-cache-dir ouroboros-cli

RUN useradd -lmrs /usr/sbin/nologin ouroboros
ADD entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
